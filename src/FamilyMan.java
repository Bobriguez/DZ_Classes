/**
 * Created by Bobriguez on 01.09.2018.
 */
public class FamilyMan extends Human{

    private String role;

    FamilyMan(String name, char gender, int age, String role){
        super(name,gender,age);
        this.role = role;
    }

    public void Info(){
        System.out.println("Информация о члене семьи:");
        System.out.println("Имя: " + name + ", Пол: " + gender + ", Возраст: "+ age + ", Роль в семье: " + role);
        System.out.println("-----------------------------------------------------------------------------------");
    }

    public static void main(String[] args) {
        FamilyMan father = new FamilyMan("АФанасий",'М',41,"Отец Кеши, Эдика и Маши");
        FamilyMan mother = new FamilyMan("Анжела",'Ж',40,"Мать Кеши, Эдика и Маши");
        FamilyMan child1 = new FamilyMan("Инокентий",'М',21,"Сын Афанасия и Анжелы");
        FamilyMan child2 = new FamilyMan("Эдуард",'М',15,"Сын Афанасия и Анжелы");
        FamilyMan child3 = new FamilyMan("Мария",'Ж',11,"Дочь Афанасия и Анжелы");
        FamilyMan grandpa1 = new FamilyMan("Николай",'М',62,"Отец Афанасия. Дед Кеши, Эдика и Маши");
        FamilyMan grandpa2 = new FamilyMan("Назар",'М',60,"Отец Анжелы. Дед Кеши, Эдика и Маши");
        FamilyMan grandma1 = new FamilyMan("Ефросиня",'Ж',71,"Мать Афанасия. Бабушка Кеши, Эдика и Маши");
        FamilyMan grandma2 = new FamilyMan("Клава",'Ж',67,"Мать Анжелы. Бабушка Кеши, Эдика и Маши");
        FamilyMan[] mas = {father, mother, child1, child2, child3, grandpa1, grandma1, grandpa2, grandma2};
        for (FamilyMan i: mas){
            i.Info();
        }
    }
}
