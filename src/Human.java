/**
 * Created by Bobriguez on 01.09.2018.
 */
public class Human {

    protected String name;
    protected char gender;
    protected int age;

    Human(){
        this.name = "Иван";
        this.gender = 'М';
        this.age = 35;
    }

    Human(String name, char gender, int age){
        this.name = name;
        this.gender = gender;
        this.age = age;
    }
}
